<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Q&A: Event Registration</title>
	<link rel="stylesheet" href="./bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" href="./styles/main.css">
	<script src="./js/jquery-3.4.1.js"></script>
	<style>
		.form-container{
			text-align: center;
			width: 50%;;
			border: 1px solid black;
			margin-top: 10px;
			border-radius: 20px;
		}
	</style>
</head>
<body>
	<div class="container-fluid cust-container-1 text-center">
		<?php include "./navbar.html";?>
		<div class="form-container form-group">
			<div class="mx-auto pt-3 my-4">
				<img src="" alt="Logo Web" class="logo-size">
			</div>
			<form action="./event0.php" method="post">
					<div class="form-group col-md-12 text-left">
						<label for="">Event Name</label>
						<input type="text" name="eName" class="form-control">
					</div>
					<div class="form-group row col-md-12 text-left mx-auto">
						<div class="w-50">
							<label for="">Start Date</label>
							<input type="date" name="sDate" class="form-control">
						</div>
						<div class="w-50">
							<label for="">End Date</label>
							<input type="date" name="eDate" class="form-control">
						</div>
					</div>
					<div class="form-group row col-md-12 text-left mx-auto">
						<div class="w-50">
							<label for="">Start Time</label>
							<input type="time" name="sTime" class="form-control">
						</div>
						<div class="w-50">
							<label for="">End Time</label>
							<input type="time" name="eTime" class="form-control">
						</div>
					</div>
					<div class="form-group col-md-12 text-left">
						<label for="">Venue</label>
						<input type="text" name="eVenue" class="form-control">
					</div>
					<div class="form-group col-md-12 text-left">
						<label for="">Organizer Event</label>
						<input type="text" name="eOrg" class="form-control">
					</div>
					<div class="form-group col-md-12 text-left">
						<label for="">Event's Detail</label>
						<textarea class="form-control" name="eDetail" rows="5"></textarea>
					</div>
					<div class="my-4">
						<button type="submit" name="register-event" value="submit" class="btn btn-info">Submit</button>
					</div>
			</form>
		</div>
	</div>
</body>
</html>
