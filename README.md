## About
This project was made as one of the assignments in the class CSC318 that was instructed Sir Zulazeze Bin Tusiran @ Sahri. The class teaches us the basic of HTML and PHP. It is a group project that consist of 5 people which is lead by @nurhaziq97. This project is made to ease the process of Question and Answer(Q&A) session after an event or a meeting that includes a crowd of people. This project uses a Like system where the most liked question will be transferred to the top of the list of questions.

## Group Member
@akmalsyampro99
@ainvauxh
@darkskynet9
@haikalhaizad990808
