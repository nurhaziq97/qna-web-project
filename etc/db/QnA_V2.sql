-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.38-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             10.1.0.5484
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for qna
CREATE DATABASE IF NOT EXISTS `qna` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `qna`;

-- Dumping structure for table qna.adminevent
CREATE TABLE IF NOT EXISTS `adminevent` (
  `idadminevent` int(11) NOT NULL,
  `user_iduser` int(11) DEFAULT NULL,
  `event_idevent` int(11) DEFAULT NULL,
  PRIMARY KEY (`idadminevent`),
  KEY `adminevent_event` (`event_idevent`),
  KEY `adminevent_user` (`user_iduser`),
  CONSTRAINT `adminevent_event` FOREIGN KEY (`event_idevent`) REFERENCES `event` (`idevent`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `adminevent_user` FOREIGN KEY (`user_iduser`) REFERENCES `user` (`iduser`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table qna.adminevent: ~0 rows (approximately)
/*!40000 ALTER TABLE `adminevent` DISABLE KEYS */;
/*!40000 ALTER TABLE `adminevent` ENABLE KEYS */;

-- Dumping structure for table qna.chat
CREATE TABLE IF NOT EXISTS `chat` (
  `idchat` int(11) NOT NULL AUTO_INCREMENT,
  `eparticipant_idepart` int(11) DEFAULT NULL,
  `textchat` longtext COLLATE utf8_unicode_ci,
  `status_permission` int(11) NOT NULL,
  `admin_permission` int(11) NOT NULL,
  PRIMARY KEY (`idchat`),
  KEY `chat_epart` (`eparticipant_idepart`),
  KEY `chat_status` (`status_permission`),
  KEY `chatpermission` (`admin_permission`),
  CONSTRAINT `chat_epart` FOREIGN KEY (`eparticipant_idepart`) REFERENCES `eparticipant` (`ideparticipant`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `chat_status` FOREIGN KEY (`status_permission`) REFERENCES `status` (`idstatus`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `chatpermission` FOREIGN KEY (`admin_permission`) REFERENCES `adminevent` (`idadminevent`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table qna.chat: ~0 rows (approximately)
/*!40000 ALTER TABLE `chat` DISABLE KEYS */;
/*!40000 ALTER TABLE `chat` ENABLE KEYS */;

-- Dumping structure for table qna.eparticipant
CREATE TABLE IF NOT EXISTS `eparticipant` (
  `ideparticipant` int(11) NOT NULL,
  `partemail` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `event_idevent` int(11) NOT NULL,
  PRIMARY KEY (`ideparticipant`),
  KEY `event_participant` (`event_idevent`),
  CONSTRAINT `event_participant` FOREIGN KEY (`event_idevent`) REFERENCES `event` (`idevent`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table qna.eparticipant: ~0 rows (approximately)
/*!40000 ALTER TABLE `eparticipant` DISABLE KEYS */;
/*!40000 ALTER TABLE `eparticipant` ENABLE KEYS */;

-- Dumping structure for table qna.event
CREATE TABLE IF NOT EXISTS `event` (
  `idevent` int(11) NOT NULL AUTO_INCREMENT,
  `eventname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sEvent` datetime DEFAULT NULL,
  `eEvent` datetime DEFAULT NULL,
  `venueEvent` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_idstatus` int(11) NOT NULL,
  `orgEvent` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descEvent` longtext COLLATE utf8_unicode_ci,
  `eventcol` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idevent`),
  KEY `status_event` (`status_idstatus`),
  CONSTRAINT `status_event` FOREIGN KEY (`status_idstatus`) REFERENCES `status` (`idstatus`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table qna.event: ~0 rows (approximately)
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
/*!40000 ALTER TABLE `event` ENABLE KEYS */;

-- Dumping structure for table qna.status
CREATE TABLE IF NOT EXISTS `status` (
  `idstatus` int(11) NOT NULL AUTO_INCREMENT,
  `codeStatus` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idstatus`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table qna.status: ~0 rows (approximately)
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
/*!40000 ALTER TABLE `status` ENABLE KEYS */;

-- Dumping structure for table qna.user
CREATE TABLE IF NOT EXISTS `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `umail` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `usertype_id` int(11) NOT NULL,
  `ufname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ulname` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`iduser`),
  KEY `FK1_usertype_user` (`usertype_id`),
  CONSTRAINT `FK1_usertype_user` FOREIGN KEY (`usertype_id`) REFERENCES `usertype` (`idusertype`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table qna.user: ~0 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table qna.usertype
CREATE TABLE IF NOT EXISTS `usertype` (
  `idusertype` int(11) NOT NULL AUTO_INCREMENT,
  `typename` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `typecode` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idusertype`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table qna.usertype: ~4 rows (approximately)
/*!40000 ALTER TABLE `usertype` DISABLE KEYS */;
INSERT INTO `usertype` (`idusertype`, `typename`, `typecode`) VALUES
	(1, 'Admin', 'A'),
	(2, 'Admin Event', 'AE'),
	(3, 'User', 'U'),
	(4, 'Temp User', 'TU');
/*!40000 ALTER TABLE `usertype` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
